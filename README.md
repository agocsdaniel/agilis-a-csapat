Git-based F2P Network
=====================

Most of the requirements of the F2P network can be met using git.
This is a short Proof-of-Concept to show how.

How to run the demo?
--------------------

```
cd images
docker-compose up -d
```

The compose file launches two peer containers besides the server. To access the CLI, use

`docker exec -it images_node1_1 f2p`

Then in the client terminal the command interpreter can be used.

Git Server
----------

The central server will act as the git server hosting the repository.
Use the `gitserver.sh` script to create the repository and start serving.

Git Client
----------

The git repository can be cloned (almost) like any other.
Just run `git clone http://localhost:8000/cgi-bin/git peers`.

To interact with the git commands from Python, simply call
`subprocess.run()` with the appropriate parameters.

Running tests
----------

To run the tests execute the following command from the root directory:
`python3 -m pytest ./images/node/f2ppeer`

