# flake8: noqa
#
# A command line interpreter.
#
from cmd import Cmd
from functools import wraps
import inspect
from itertools import chain, islice, tee
import string


# Quick help:
#   - Methods starting with do_ will be interpreted as commands.
#   - The docstrings of those commands will be their help message.
#   - String type annotations of method parameters serve as parameter help.
class F2PConsoleBase(Cmd):
    # ANSI escape sequences are used to produce fancy terminal output.
    # Reference: https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
    ANSI_BLACK = '\033[30m'
    ANSI_RED = '\033[31m'
    ANSI_GREEN = '\033[32m'
    ANSI_YELLOW = '\033[33m'
    ANSI_BLUE = '\033[34m'
    ANSI_MAGENTA = '\033[35m'
    ANSI_CYAN = '\033[36m'
    ANSI_WHITE = '\033[37m'
    ANSI_RESET = '\033[0m'
    ANSI_BOLD = '\033[1m'
    ANSI_NONBOLD = '\033[22m'
    ANSI_CLEAR = '\033[2J'
    ANSI_HOME = '\033[H'

    def __init__(self):
        super().__init__()

        self.prompt_text = getattr(self, 'prompt_text', 'pwn')
        self.prompt = self.ANSI_YELLOW + self.ANSI_BOLD + self.prompt_text + ' > ' + self.ANSI_RESET
        self.nohelp = ' No description for %s.'

        for func in dir(self):
            if func.startswith('do_'):
                setattr(self, func, self._argsdecorator(getattr(self, func)))

        # The following builds a command prefix lookup table
        # that can be used during subcommand parsing.
        self.cmdprefix = dict()
        for func in dir(self):
            func = func.split('_')
            if func[0] == 'do':
                lvl = self.cmdprefix
                for subcmd in func[1:]:
                    if subcmd not in lvl:
                        lvl[subcmd] = dict()
                    lvl = lvl[subcmd]

    # Default help would only output method docstrings.
    # This version will also parse the function signature to
    # determine the required parameters and the returned values.
    def do_help(self, *cmd: 'command to get help on'):
        """ List available commands with "help" or detailed help with "help cmd". """

        # Helper utilities.
        def take(n, iterable):
            return list(islice(iterable, n))

        def spy(iterable, n=1):
            it = iter(iterable)
            head = take(n, it)
            return head.copy(), chain(head, it)

        def unzip(iterable):
            head, iterable = spy(iter(iterable))
            if not head:
                return ()
            head = head[0]
            iterables = tee(iterable, len(head))

            def itemgetter(i):
                def getter(obj):
                    try:
                        return obj[i]
                    except IndexError:
                        raise StopIteration

                return getter

            return tuple(map(itemgetter(i), it) for i, it in enumerate(iterables))

        # Return the parameter help split into left and right side.
        def _param_to_str(param):
            left = f'   - {param.name}'
            if param.default is not inspect._empty:
                left += f' = {repr(param.default)}'
            if param.annotation is not inspect._empty:
                right = f'  | {param.annotation}'
            else:
                right = ''
            return left, right

        print()
        if cmd:
            cmd = '_'.join(cmd)
            super().do_help(cmd)
            if hasattr(self, 'do_' + cmd):
                sig = inspect.signature(getattr(self, 'do_' + cmd))
                if sig.parameters:
                    print(' Parameters:')
                    lefts, rights = zip(*map(_param_to_str, sig.parameters.values()))
                    for l, r in unzip([lefts, rights]):
                        print(l + ' ' * (max(map(len, lefts)) - len(l)) + r)
                else:
                    print(' The command takes no arguments.')
            print()
        else:
            print(self.__class__.__name__ + ':')
            print('=' * (len(self.__class__.__name__) + 1))
            for cls in (*self.__class__.__bases__, self.__class__):
                if cls is Cmd:
                    continue
                for func in sorted(dir(cls)):
                    if func.startswith('do_'):
                        doc = getattr(cls, func).__doc__
                        func = ' '.join(func.split('_')[1:])
                        print(func + ' ' * (10 - len(func)) + f'| {doc}')
            print()

    # Splits the user input (line read from console).
    # This is here to support subcommands without underscores:
    # i.e. user can input "cmd subcmd param1" instead of "cmd_subcmd param1".
    def parseline(self, line):
        line = line.strip()
        if not line:
            return None, None, line
        subcmds = [[0, '']]
        for i, char in enumerate(line):
            if char in string.whitespace:
                if subcmds[-1][1] == '':
                    subcmds[-1][0] += 1
                else:
                    subcmds.append([subcmds[-1][0] + 1, ''])
            else:
                subcmds[-1][0] += 1
                subcmds[-1][1] = subcmds[-1][1] + char
        lvl = self.cmdprefix
        for j, subcmd in enumerate(subcmds):
            if subcmd[1] in lvl:
                lvl = lvl[subcmd[1]]
            else:
                j -= 1
                break
        return '_'.join(s[1] for s in subcmds[:j + 1]), line[subcmds[j][0]:].strip(), line

    def default(self, line):
        self.error(f'Unknown command: {line}')

    def error(self, msg='Wrong syntax.'):
        print(self.ANSI_RED + f'\n{msg}\n' + self.ANSI_RESET)

    # Argsdecorator is used to handle argument verification.
    # It wraps the command methods and outputs error messages in case of wrong usage.
    def _argsdecorator(self, f):
        @wraps(f)
        def wrapper(args):
            try:
                return f(*args.split())
            except TypeError as e:
                # TODO: This is a rough estimate of a type error.
                # Ideally the type hints should be read from the method's signature
                # and all parameters should be converted to the required types (if
                # not already matching). If that conversion fails, then the types are
                # definitely wrong. Here, it could by any TypeError...
                print(self.ANSI_RED + '\nWrong arguments!\n' + self.ANSI_RESET + '\nUsage:')
                self.do_help(f.__name__[3:])
            except Exception as e:
                print(self.ANSI_RED + f'Exception encountered:\n{e}\n' + self.ANSI_RESET)

        return wrapper

