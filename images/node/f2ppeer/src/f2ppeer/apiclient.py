import requests
import json
from f2ppeer.apiserver import route_nodes, route_read, route_write, flask_port

base_url = f"http://localhost:{flask_port}"

node_url = f"{base_url}{route_nodes}"
read_url = f"{base_url}{route_read}"
write_url = f"{base_url}{route_write}"


def __post_request(uri, data):
    result = requests.post(uri, json=json.dumps(data))
    if result.status_code >= 200 and result.status_code < 400:
        return json.loads(result.text)
    else:
        raise Exception(f"Invalid return code from uri {uri}: {result.status_code}")


def __get_request(uri, params=None):
    result = requests.get(uri, params=params)
    if result.status_code >= 200 and result.status_code < 400:
        return json.loads(result.text)
    else:
        raise Exception(f"Invalid return code from uri {uri}: {result.status_code}")


def write(peer, entry_id, text):
    write_request_params = {
        'peer': peer,
        'entry_id': entry_id,
        'text': text
    }
    return __post_request(write_url, write_request_params)["location"]


def read(peer, start, end):
    read_request_params = {
        'peer': peer,
        'start': start,
        'end': end
    }
    record = __get_request(read_url, read_request_params)
    read_data = []
    for content in record["content"]:
        read_data.append(f"{content['id']}: {content['value']}")
    return read_data


def nodes():
    return __get_request(node_url)["nodes"]
