import subprocess
import os
import time
import random


# =============================================================================
# Command implementation functions.
# =============================================================================
BASE_DIR = '/tmp/fake2peer'
PEERS_DIR = os.path.join(BASE_DIR, 'peers')

peerid = os.getenv('AGILIS_PEERID', "default")
host = os.environ.get("AGILIS_HOST", 'http://localhost:8000/cgi-bin/git')


def _silent_run(*args, cwd=BASE_DIR, accepted_errors=[]):
    with subprocess.Popen(args, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as p:
        p.wait()
        if p.returncode != 0 and p.returncode not in accepted_errors:
            print(args)
            print(p.returncode)
            for line in p.stdout:
                print(line.decode("utf-8"))
            for line in p.stderr:
                print(line.decode("utf-8"))
        return p.stdout.read().decode()


def _sync():
    # Discard everything not yet committed.
    _silent_run('git', 'reset', '--hard')
    _silent_run('git', 'clean', '-fxd', ':/')
    # Fetch and rebase changes.
    _silent_run('git', 'pull', '-ff')


def _register():
    _silent_run('mkdir', '-p', '_meta/registered')
    peerfile = f'peers/{peerid}'
    if not os.path.isfile(peerfile):
        print(f"Node has no file, creating {peerfile}")
        _silent_run('touch', peerfile)
        _silent_run('git', 'add', peerfile)
    _silent_run('touch', f'_meta/registered/{peerid}')
    _silent_run('git', 'add', f'_meta/registered/{peerid}')
    _silent_run('git', 'commit', '-m', f'Register peer {peerid}', accepted_errors=[1])
    _silent_run('git', 'pull', '--rebase')
    _silent_run('git', 'push', 'origin', 'master')


def _unregister():
    _silent_run('rm', f'_meta/registered/{peerid}', accepted_errors=[1, 128])
    _silent_run('git', 'add', f'_meta/registered/{peerid}', accepted_errors=[128])
    _silent_run('git', 'commit', '-m', f'Deregister peer {peerid}', accepted_errors=[1])
    _silent_run('git', 'push', 'origin', 'master')


def init_repo():
    _silent_run('rm', '-rf', BASE_DIR, cwd='.')
    _silent_run('git', 'clone', '--branch', 'master', host, BASE_DIR, cwd='.', accepted_errors=[128])
    _silent_run('git', 'clone', host, BASE_DIR, cwd='.', accepted_errors=[128])
    os.makedirs(PEERS_DIR, exist_ok=True)
    _silent_run('git', 'config', '--local', 'user.name', peerid)
    _silent_run('git', 'config', '--local', 'user.email', 'None')
    _silent_run('git', 'config', '--local', 'init.defaultBranch', 'master')
    _silent_run('git', 'checkout', '-b', 'master', accepted_errors=[128])
    if not os.path.exists(PEERS_DIR):
        os.makedirs(PEERS_DIR)
        _silent_run('touch', 'peers/.gitkeep')
        _silent_run('git', 'add', 'peers/.gitkeep')
        _silent_run('git', 'commit', '-m', f'Init peers dir {peerid}')
    _register()
    _silent_run('git', 'branch', '-u', 'origin/master')


def on_quit():
    _unregister()
    _silent_run('rm', '-Rf', BASE_DIR, cwd='.')


def read(filepath, start=0, stop=None):
    """ Print the file's lines from start to stop. """
    read_content = []
    _sync()
    try:
        with open(os.path.join(PEERS_DIR, filepath)) as f:
            records = f.read().split("\n")
        if stop is None or stop > len(records):
            stop = len(records)
        index = start
        for line in records[start:stop]:
            if line:
                read_content.append((index, line))
                index += 1
    except FileNotFoundError:
        read_content = [f"No registered node with name {filepath}"]
    return read_content


def nodes():
    return os.listdir(PEERS_DIR)


def write(filepath, location, value):
    """
    Write the value to the file at the specified location.
    If location is None it will append to the file.
    If value is None the record will be "deleted" (overwritten with empty string).
    If location does not exist yet, fills up to the location
    Returns the affected line number.
    """
    _sync()
    if not os.path.exists(PEERS_DIR):
        os.makedirs(PEERS_DIR, exist_ok=True)
        _silent_run("git", "add", PEERS_DIR)
    with open(os.path.join(PEERS_DIR, filepath), 'a+') as f:
        f.seek(0)
        records = f.readlines()
    if type(location) is int:
        if location >= len(records):
            for i in range(location - len(records) + 1):
                records.append('\n')
        records[location] = (value or '') + '\n'
    else:
        location = len(records)
        records.append(value + '\n')
    with open(os.path.join(PEERS_DIR, filepath), 'w') as f:
        f.writelines(r for r in records)
    _silent_run('git', 'add', os.path.join('peers', filepath))
    _silent_run('git', 'commit', '-m', 'placeholder')
    _silent_run('git', 'push')
    return location


def poll():
    while True:
        time.sleep(10)
        _sync()


def sensor(name):
    while True:
        time.sleep(1)
        new_data = random.random()
        write(name, None, str(new_data))
