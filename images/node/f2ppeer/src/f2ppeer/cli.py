#!/usr/bin/env python3
#
# CLI demonstration of a git Python interface.
#
from sys import exit
import threading

from f2ppeer.protocol import poll
from f2ppeer.console import F2PConsole


def main():
    try:
        # Polling will pull changes from the git server.
        polling = threading.Thread(target=poll, daemon=True)
        polling.start()

        # Run the command interpreter.
        F2PConsole().cmdloop()
    except KeyboardInterrupt:
        exit()
    finally:
        pass


if __name__ == '__main__':
    main()
