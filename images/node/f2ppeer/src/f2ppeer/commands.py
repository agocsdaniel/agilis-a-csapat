#
# Protocol implementation.
#
from sys import exit

from f2ppeer.apiclient import write, read, nodes


# =============================================================================
# Command declarations.
# =============================================================================
class F2PClientApi:
    def append_command(self, peer, text):
        location = write(peer, None, text)
        print(f'\nRecord with id {location} successfully written.\n')

    def delete_command(self, peer, idx):
        write(peer, int(idx), None)
        print(f'\nRecord with id {idx} successfully deleted.\n')

    def edit_command(self, peer, idx, text):
        write(peer, int(idx), text)
        print(f'\nRecord with id {idx} successfully overwritten.\n')

    # TODO: A raw sys.exit is not ideal, but it works.
    #   - Threads must be joined.
    #   - Terminal attributes (e.g. color) must be reset.
    # If do_exit returns True, the cmdloop will stop!
    def exit_command(self):
        exit()

    def node_command(self):
        print()
        print('\n'.join(nodes()))
        print()

    def show_command(self, peer, i, j):
        if i == 'first':
            i = 0
        if j == 'last':
            j = -1
        print()
        print('\n'.join(read(peer, int(i), int(j))))
        print()

    # TODO: Could add a sensor_stop command as well?
    def sensor_start_command(self):
        # TODO: This command could even be called in gitclient.sh automatically?
        # TODO: Multiple sensor should probably not be started on the same peer...
        # peerid = os.getenv('AGILIS_PEERID')
        # st = threading.Thread(target=sensor, args=(peerid,), daemon=True)
        # st.start()
        # print(f'\nSensor {peerid} initiated successfully.\n')
        print("This feature is unavailable at the moment")
