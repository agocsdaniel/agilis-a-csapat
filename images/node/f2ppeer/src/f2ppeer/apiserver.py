import threading
from flask import Flask, request
from json import loads
from .protocol import _register, _unregister, init_repo, read, write, nodes, poll

app = Flask(__name__)

route_read = "/read"
route_write = "/write"
route_nodes = "/nodes"
flask_port = 5000


@app.route(route_read, methods=['GET'])
def req_read():
    peer = request.args.get("peer", type=str)
    start = request.args.get("start", type=int)
    end = request.args.get("end", type=int)
    if end == -1:
        end = None
    members = []
    for k, v in read(peer, start, end):
        members.append({
            'id': k,
            'value': v
        })
    return {"content": members}


@app.route(route_write, methods=['POST'])
def req_write():
    write_request = loads(request.get_json())
    location = write(write_request["peer"],
                     write_request["entry_id"],
                     write_request["text"])
    return {"location": location}


@app.route(route_nodes, methods=['GET'])
def req_nodes():
    return {"nodes": nodes()}


def main():
    init_repo()
    print("Initialized local repository")
    _register()
    polling = threading.Thread(target=poll, daemon=True)
    polling.start()
    try:
        app.run(debug=True, host="0.0.0.0", port=flask_port)
    except KeyboardInterrupt:
        _unregister()


if __name__ == "__main__":
    main()
