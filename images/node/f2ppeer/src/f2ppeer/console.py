# flake8: noqa
#
# A command line interpreter.
#
import os
from f2ppeer.consolebase import F2PConsoleBase
from f2ppeer.commands import F2PClientApi


# Quick help:
#   - Methods starting with do_ will be interpreted as commands.
#   - The docstrings of those commands will be their help message.
#   - String type annotations of method parameters serve as parameter help.
class F2PConsole(F2PConsoleBase):
    def __init__(self):
        # Intro is what greets the user when they start the command interpreter.
        # Generated with https://patorjk.com/software/taag/
        # Current style: ANSI shadow
        self.intro = self.ANSI_CLEAR + self.ANSI_HOME + '''
        ███████╗ █████╗ ██╗  ██╗███████╗██████╗ ██████╗ ███████╗███████╗██████╗
        ██╔════╝██╔══██╗██║ ██╔╝██╔════╝╚════██╗██╔══██╗██╔════╝██╔════╝██╔══██╗
        █████╗  ███████║█████╔╝ █████╗   █████╔╝██████╔╝█████╗  █████╗  ██████╔╝
        ██╔══╝  ██╔══██║██╔═██╗ ██╔══╝  ██╔═══╝ ██╔═══╝ ██╔══╝  ██╔══╝  ██╔══██╗
        ██║     ██║  ██║██║  ██╗███████╗███████╗██║     ███████╗███████╗██║  ██║
        ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     ╚══════╝╚══════╝╚═╝  ╚═╝
        '''

        self.prompt_text = f'f2p@{os.getenv("AGILIS_PEERID")}'

        super().__init__()

        self.api = F2PClientApi()

    # =========================================================================
    # The following is the most important part of the console.
    # These are the F2P command declarations.
    # These just pass the parameters to the cmd_ functions imported above.
    # =========================================================================

    def do_append(self, peer: 'name of the peer', text: 'text of the record'):
        """ Add a new record to a peer. """
        self.api.append_command(peer, text)

    def do_delete(self, peer: 'name of the peer', idx: 'number of the record'):
        """ Remove a record from a peer. """
        self.api.delete_command(peer, idx)

    def do_edit(self,
                peer: 'name of the peer',
                idx: 'number of the record',
                text: 'new text of the record'
                ):
        """ Overwrite a record of a peer. """
        self.api.edit_command(peer, idx, text)

    def do_exit(self):
        """ Exit the program. """
        self.api.exit_command()

    def do_show(self, peer: 'name of the peer', i: 'start' = 'first', j: 'end' = 'last'):
        """ Display record contents. If i and j are not specified, show all records. """
        self.api.show_command(peer, i, j)

    def do_sensor_start(self):
        """ Start the sensor on this peer. Will generate 1 message / second. """
        self.api.sensor_start_command()

    def do_nodes(self):
        """Lists nodes in the system"""
        self.api.node_command()
