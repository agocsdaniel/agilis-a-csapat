import os
import time
import unittest
import subprocess
import signal

from f2ppeer.protocol import init_repo, _sync, _silent_run, _unregister


class TestMethods(unittest.TestCase):

    p = None

    @classmethod
    def setUpClass(cls):
        command = [os.getcwd() + "/images/server/gitserver.sh"]
        cls.p = subprocess.Popen(command, cwd="/tmp/", preexec_fn=os.setsid, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        time.sleep(1)
        print("\n")

    @classmethod
    def tearDownClass(cls):
        os.killpg(os.getpgid(cls.p.pid), signal.SIGINT)
        cls.p.terminate()

    def test_init(self):
        print("Testing repository initializing")
        init_repo()
        output = _silent_run("git", "show-branch").strip().lower()
        self.assertEqual(output, "[master] register peer default", "git init failed")

    def test_sync(self):
        print("Testing repository synchronization")
        _sync()
        output = _silent_run("git", "pull").strip().lower()
        self.assertEqual(output, "already up to date.", "Synchronization was successfully with the server")

    def test_unregister(self):
        print("Testing unregistering node")
        _unregister()
        output = _silent_run("git", "show-branch").strip().lower()
        self.assertEqual(output, "[master] deregister peer default", "git init failed")

    def test_on_quit(self):
        print("Testing quitting node")
        _unregister()
        output = _silent_run("git", "show-branch").strip().lower()
        self.assertEqual(output, "[master] deregister peer default", "git init failed")


if __name__ == '__main__':
    unittest.main()
