import sys
import os
dir = os.path.abspath(os.path.realpath(os.path.dirname(__file__) + "/../src/"))
if dir not in sys.path:
    sys.path.append(dir)
