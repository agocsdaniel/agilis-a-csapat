#!/usr/bin/env python3
#
# CLI demonstration of a git Python interface.
#
from sys import exit
import threading

from f2ppeer.protocol import poll, on_quit
from f2ppeer.console import F2PConsole


if __name__ == '__main__':
    try:
        # Polling will pull changes from the git server.
        polling = threading.Thread(target=poll, daemon=True)
        polling.start()

        # Run the command interpreter.
        F2PConsole().cmdloop()
    except KeyboardInterrupt:
        exit()
    finally:
        on_quit()
        pass  # TODO: Cleanup here!
