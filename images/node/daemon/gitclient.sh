#!/usr/bin/env bash
#
# Execute on a peer
#

# Fail on error.
set -e

# Generate a unique peer ID.
peerid=$(python3  -c 'import uuid; print(uuid.uuid1())')
#export AGILIS_PEERID=$peerid
export AGILIS_PEERID="${AGILIS_PEERID:-NODE1}"

./main.py
