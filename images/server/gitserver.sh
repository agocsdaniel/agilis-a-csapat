#!/usr/bin/env bash
#
# A super simple HTTP git server:
# [1](https://stackoverflow.com/a/47797885)
#
# Requires only git and Python 3.
#

# Fail on error.
set -e

# Setup.
rm -rf fake2peer.git
mkdir fake2peer.git
cd fake2peer.git
git init --bare || true

# Ignore global git configuration.
unset GIT_CONFIG
git config --local http.receivepack true
git config --local init.defaultBranch 'master'

# Start git HTTP server.
touch git-daemon-export-ok
mkdir cgi-bin || true
ln -s /usr/lib/git-core/git-http-backend cgi-bin/git || true
git update-server-info
python3 -c '
import http.server
http.server.CGIHTTPRequestHandler.have_fork = False
http.server.test(HandlerClass=http.server.CGIHTTPRequestHandler, port=8000)'
