import subprocess
from os import environ, path, getcwd, remove
import argparse
from urllib.parse import urlparse

pipconf = """[global]
index-url = URL
trusted-host = BASE
               pypi.org
extra-index-url= http://pypi.org/simple"""

def gen_conf(out_path):
    _user = environ["PYPIUSER"]
    _pass = environ["PYPIPASS"]
    _repo = environ["PYPIREPO"]

    auth = f"{_user}:{_pass}@"

    parsed = urlparse(_repo)
    
    conf = pipconf.replace("URL", f"{parsed.scheme}://{auth}{parsed.netloc}{parsed.path}").replace("BASE", parsed.netloc)
    with open(out_path, 'w') as f:
        f.write(conf)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", required=True)
    parser.add_argument("--pipconf-id", required=True)
    parser.add_argument("--image", required=True)
    args = parser.parse_args()
    secret_source = path.join(getcwd(), args.dir, "pip.conf")

    gen_conf(secret_source)
    command = [
        "docker",
        "build",
        "--no-cache",
        "--rm=true",
        "--secret",
        f"id={args.pipconf_id},src={secret_source}",
        "-t",
        args.image,
        args.dir
    ]
    env = environ.copy()
    env["DOCKER_BUILDKIT"] = "1"
    with subprocess.Popen(command, env=env) as p:
        p.wait()
    