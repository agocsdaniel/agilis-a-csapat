from os import environ
from urllib.parse import urlparse

pipconf = """[global]
index-url = URL
trusted-host = BASE
               pypi.org
extra-index-url= http://pypi.org/simple"""

def gen_conf(out_path):
    _user = environ["PYPIUSER"]
    _pass = environ["PYPIPASS"]
    _repo = environ["PYPIREPO"]

    auth = f"{_user}:{_pass}@"

    parsed = urlparse(_repo)
    print(parsed)
    
    conf = pipconf.replace("URL", f"{parsed.scheme}://{auth}{parsed.netloc}{parsed.path}").replace("BASE", parsed.netloc)
    with open(out_path, 'w') as f:
        f.write(conf)
    print(conf)